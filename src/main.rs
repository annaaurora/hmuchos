// This file is part of the hmuchos source code.
//
// ©️ 2022 papojari <mailto:papojari-git.ovoid@aleeas.com> <https://matrix.to/#/@papojari:artemislena.eu> <https://papojari.codeberg.page>
// ©️ 2022 Xiretza <https://github.com/Xiretza> <https://matrix.to/#/@xiretza:xiretza.xyz>
//
// For the license information, please view the README.md file that was distributed with this source code.

#![warn(clippy::pedantic)]

use clap::Parser;
use unit_conversions::mass;
use colorful::Colorful;
use colorful::Color;

#[derive(Parser, Debug)]
#[clap(name = "hmuchos", version, about="mass in gram units to mass in cow, banana, tomato, …")]
struct Args {
    /// Mass value
    #[clap(long, short)]
    value: f64,
    /// Mass unit
    #[clap(long, short, possible_values = ["mg", "g", "kg"], default_value = "g")]
    unit: String,
    /// If the output should be just the result or also show the input.
    #[clap(long, short = 'r')]
    only_result: bool,
}

fn main() {
    let args = Args::parse();
    let unit = args.unit;
    let value = args.value;

    #[allow(clippy::non_ascii_literal)]
    let masses = [
        ("🐄", 900_000.0),
        ("🍌", 120.0),
        ("🍎", 195.0),
        ("🥑", 170.0),
        ("🥥", 680.0),
        ("🍍", 1590.0),
        ("🎃", 4500.0),
        ("🥒", 250.0),
        ("🥔", 184.0),
        ("🍅", 170.0),
        ("🌽", 180.0),
    ];

    // Convert value which can be in either mg, g or kg defined by unit to gram.
    let value_to_grams = match unit.as_str() {
        "mg" => mass::milligrams::to_grams(value),
        "g" => mass::kilograms::to_grams(mass::grams::to_kilograms(value)),
        "kg" => mass::kilograms::to_grams(value),
        _ => panic!(),
    };

    if args.only_result {
    } else {
        println!("{} {} {}", value.to_string().color(Color::Yellow).bold().underlined(), unit.color(Color::Green).bold(), "are:" );
        println!();
    };

    // Print how many times the input_mass fits into mass for every single mass in masses.
    for (emoji, mass) in masses {
        println!("{} {}", value_to_grams / mass, emoji);
    };
}
