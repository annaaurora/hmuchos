# hmuchos 🏋️

hmuchos asks for an input mass in gram. Then it prints how many times that mass fits into the mass of an average cow, banana or tomato.

## License

©️ 2022 papojari <mailto:papojari-git.ovoid@aleeas.com> <https://matrix.to/#/@papojari:artemislena.eu> <https://papojari.codeberg.page>  
©️ 2022 Xiretza <https://github.com/Xiretza> <https://matrix.to/#/@xiretza:xiretza.xyz>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License version 3 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License version 3 along with this program. If not, see <https://www.gnu.org/licenses/>.
